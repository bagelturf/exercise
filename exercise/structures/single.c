//
//  single.c
//  exercise
//
//  Created by Steve Weller on 3/10/19.
//  Copyright © 2019 Steve Weller. All rights reserved.
//

#include "single.h"

#include <stdlib.h>
#include <stdio.h>

single_node_p create_node(void)
{
	return calloc(sizeof(single_node_t), 1);
}

void destroy_node(single_node_p node)
{
	free(node);
}

void push_head(single_node_p *h, single_node_p *t, element value)
{
	single_node_p new =create_node();
	new->v = value;
	
	if(!*h) {
		*h = new;
		*t = new;
		return;
	}
	
	new->n = *h;
	*h = new;
}


int pop_head(single_node_p *h, single_node_p *t, element *value)
{
	single_node_p node = *h;
	if(!node) return 0;
	
	*value = (*h)->v;
	
	*h = (*h)->n;
	destroy_node(node);
	
	if(*h==NULL) *t = NULL;
	
	return 1;
}

void push_tail(single_node_p *h, single_node_p *t, element value)
{
	single_node_p new =create_node();
	new->v = value;
	
	if(!*h) {
		*h = new;
		*t = new;
		return;
	}
	
	(*t)->n = new;
	*t = new;
}



void enumerate_list_single(single_node_p *h, single_node_p *t, enumerate_list_b block)
{
	single_node_p node = *h;
	while(node) {
		block(node);
		node = node->n;
	};
}


void enumerate_list_single_recursive(single_node_p *h, single_node_p *t, enumerate_list_b block)
{
	if(!(*h)) return;
	block(*h);
	enumerate_list_single_recursive(&(*h)->n, t, block);
}


void print_list(single_node_p *h, single_node_p *t)
{
	printf("Head: <%p>, tail: <%p>\n",*h,*t);
	__block int posn = 0;
	enumerate_list_single(h, t, ^(single_node_p node) {
		element value = node->v;
		printf("<%p>: %3d %15.10g\n",node, posn, value);
		posn++;
	});
	printf("\n");
}

void print_list_recursive(single_node_p *h, single_node_p *t)
{
	printf("Head: <%p>, tail: <%p>\n",*h,*t);
	__block int posn = 0;
	enumerate_list_single_recursive(h, t, ^(single_node_p node) {
		element value = node->v;
		printf("<%p>: %3d %15.10g\n",node, posn, value);
		posn++;
	});
	printf("\n");
}


