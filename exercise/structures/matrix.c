//
//  matrix.c
//  exercise
//
//  Created by Steve Weller on 3/10/19.
//  Copyright © 2019 Steve Weller. All rights reserved.
//

#include "matrix.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

element *matrix_create(int nc, int nr)
{
	element *e = (element*)calloc(nc*nr, sizeof(element));
	return e;
}


static void enumerate_by_row(element *m, int nc, int nr, matrix_enumerator_b block)
{
	for(int r=0;r<nr;r++) {
		for(int c=0;c<nc;c++) {
			block(m+r*nc+c,c,r);
		}
	}
}


void matrix_fill_value(element *m, int nc, int nr)
{
	__block int value = 0;
	enumerate_by_row(m, nc, nr, ^(element *e, int c, int r) {
		*e = value++;
	});
}

void matrix_print(element *m, int nc, int nr)
{
	__block int last_row = 0;
	enumerate_by_row(m, nc, nr, ^(element *e, int c, int r) {
		if(last_row!=r) printf("\n");
		last_row = r;
		printf("%4g ",*e);
	});
	printf("\n");
}

static void matrix_copy(element *m, int nc, int nr, element *copy)
{
	memcpy(copy, m, sizeof(element)*nr*nc);
}


//static void enumerate_by_column_bottom_to_top(element *m, int nc, int nr, matrix_enumerator_b block)
//{
//	// Each column left to right, from the bottom up
//	for(int c = 0;c<nc-1;c++) {
//		for(int r = nr-1;r>0;r--) {
//			block(m+r*nc+c,c,r);
//		}
//	}
//}


void matrix_rotate(element *m, int nc, int nr)
{
	element *copy = matrix_create(nc, nr);
	
	enumerate_by_row(m, nc, nr, ^(element *e, int c, int r) {
		int new_r = nc-1-c;
		int new_c = r;
		*(copy+new_r*nc+new_c) = *e;
	});
	
	matrix_copy(copy, nc, nr, m);
}

void matrix_rotate_in_place(element *m, int nc, int nr)
{
	
	// For each perimeter
	for(int p=0;p<nc/2;p++) {
		
		int tc = p;			// Top
		int tr = p;
		
		int rc = nc-1-p;	// Right
		int rr = p;
		
		int br = nr-1-p;	// Bottom
		int bc = nc-1-p;
		
		int lr = nr-1-p;	// Left
		int lc = p;
		
		do {
			// Rotate 4 elements with a temp
			element temp = m[tr*nc+tc];
			m[tr*nc+tc] = m[rr*nc+rc];
			m[rr*nc+rc] = m[br*nc+bc];
			m[br*nc+bc] = m[lr*nc+lc];
			m[lr*nc+lc] = temp;
			
			tc++;
			rr++;
			bc--;
			lr--;
		} while(tc<rc);
	}
}


void enumerate_perimeter(element *m, int nc, int nr, int row_inc, matrix_enumerator_b block)
{
	int r = 0;
	int c = 0;
	
	if(nr==1) {
		// Single row
		for(;c<nc;c++) {
			block(m+r*row_inc+c,c,r);
		}
		return;
	}
	
	if(nc==1) {
		// Single column
		for(;r<nr;r++) {
			block(m+r*row_inc+c,c,r);
		}
		return;
	}
	
	// Top row, right
	for(;c<nc-1;c++) {
		block(m+r*row_inc+c,c,r);
	}
	
	// Right side, down
	for(;r<nr-1;r++) {
		block(m+r*row_inc+c,c,r);
	}
	
	// Bottom, left
	for(;c>0;c--) {
		block(m+r*row_inc+c,c,r);
	}
	
	// Left side, up
	for(;r>0;r--) {
		block(m+r*row_inc+c,c,r);
	}
}
