//
//  tree.h
//  exercise
//
//  Created by Steve Weller on 3/10/19.
//  Copyright © 2019 Steve Weller. All rights reserved.
//

#ifndef tree_h
#define tree_h

#include "element.h"

typedef struct m {
	struct m *left;
	struct m *right;
	element value;
} m, *pm;

typedef void (^enumerate_tree_b)(pm node, int level);

pm create_tree_node(void);
void destroy_tree_node(pm node);
void build_tree_random_recursive(pm *root, int level, int max_depth);

void enumerate_tree_depth_recursive(pm *t, int level, enumerate_tree_b block);
void enumerate_tree_depth_stack(pm *t, int level, enumerate_tree_b block);

void print_tree_stack(pm *root);
void print_tree_recursive(pm *root);


#endif /* tree_h */
