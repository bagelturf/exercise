//
//  binarychop.h
//  exercise
//
//  Created by Steve Weller on 3/10/19.
//  Copyright © 2019 Steve Weller. All rights reserved.
//

#ifndef binarychop_h
#define binarychop_h

#include "thing.h"

#include <stdlib.h>

// -1 if 1 < 2, 0 if 1==2, +1 if 1 > 2
typedef int (^bsearch_cmp_t)(void *thing1, void *thing2);

void *bin_search(void *things, size_t size_of_each_thing, size_t number_of_things, void *target, bsearch_cmp_t block);

#endif /* binarychop_h */
