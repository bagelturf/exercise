//
//  heap.h
//  exercise
//
//  Created by Steve Weller on 3/10/19.
//  Copyright © 2019 Steve Weller. All rights reserved.
//

#ifndef heap_h
#define heap_h

#include "thing.h"

typedef struct heap {
	thing_t *items;
	int capacity;
	int used;
} heap_t;

void heap_add_thing(heap_t *heap, thing_t new_thing);
void heap_remove_thing(heap_t *heap, thing_t *thing_removed);
void heap_print(heap_t *heap);

#endif /* heap_h */
