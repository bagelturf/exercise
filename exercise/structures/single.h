//
//  single.h
//  exercise
//
//  Created by Steve Weller on 3/10/19.
//  Copyright © 2019 Steve Weller. All rights reserved.
//

#ifndef single_h
#define single_h

#include "element.h"

typedef struct single_node {
	struct single_node *n;
	element v;
} single_node_t, *single_node_p;

typedef void (^enumerate_list_b)(single_node_p node);

void push_head(single_node_p *h, single_node_p *t, element value);
int pop_head(single_node_p *h, single_node_p *t, element *value);
void push_tail(single_node_p *h, single_node_p *t, element value);

void enumerate_list_single(single_node_p *h, single_node_p *t, enumerate_list_b block);
void enumerate_list_single_recursive(single_node_p *h, single_node_p *t, enumerate_list_b block);

void print_list_recursive(single_node_p *h, single_node_p *t);
void print_list(single_node_p *h, single_node_p *t);

#endif /* single_h */
