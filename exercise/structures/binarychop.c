//
//  binarychop.c
//  exercise
//
//  Created by Steve Weller on 3/10/19.
//  Copyright © 2019 Steve Weller. All rights reserved.
//

#include "binarychop.h"


void *bin_search(void *things, size_t size_of_each_thing, size_t number_of_things, void *target, bsearch_cmp_t block)
{
	size_t lower = 0;
	size_t upper = number_of_things;
	uint8_t *t = things;
	
	do {
		size_t sample_index = lower+(upper-lower)/2;
		void *thing1 = t + size_of_each_thing*sample_index;
		void *thing2 = target;
		int result = block(thing1, thing2);
		
		if(result>0) {
			upper = sample_index;
		} else if (result<0) {
			lower = sample_index+1;
		} else {
			return thing1;
		}
		
	} while(lower<upper);
	
	// Not found
	return NULL;
}


