//
//  heap.c
//  exercise
//
//  Created by Steve Weller on 3/10/19.
//  Copyright © 2019 Steve Weller. All rights reserved.
//

#include "heap.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void heap_allocate(heap_t *heap, int capacity)
{
	heap->items = calloc(capacity, sizeof(thing_t));
	heap->capacity = capacity;
	heap->used = 0;
}

void heap_dispose(heap_t *heap)
{
	free(heap->items);
	heap->capacity = 0;
	heap->used = 0;
}

#define CHILD1(parent)  (parent*2+1)
#define CHILD2(parent)  (parent*2+2)
#define PARENT(child)  ((child-1)/2)

void heap_add_thing(heap_t *heap, thing_t new_thing)
{
	// Resize if needed
	if(heap->used==heap->capacity) {
		int new_capacity = (heap->capacity+1)*2-1;
		thing_t *new_items = calloc(new_capacity, sizeof(thing_t));
		if(heap->items) {
			memcpy(new_items, heap->items, new_capacity*sizeof(thing_t));
			free(heap->items);
		}
		heap->items = new_items;
		heap->capacity = new_capacity;
	}
	
	// Add to end
	thing_t *items = heap->items;
	items[heap->used++] = new_thing;
	
	// Bubble up as needed
	int probe = heap->used-1;
	while(probe) {
		if(items[probe].a<=items[PARENT(probe)].a) break;
		thing_t temp = items[PARENT(probe)];
		items[PARENT(probe)] = items[probe];
		items[probe] = temp;
		probe = PARENT(probe);
	};
}

void heap_remove_thing(heap_t *heap, thing_t *thing_removed)
{
	// Remove from start
	*thing_removed = heap->items[0];
	
	// Bubble down last item
	int probe = 0;
	thing_t *items = heap->items;
	items[0] = items[heap->used-1];
	heap->used--;
	while (CHILD1(probe)<heap->used) {
		int larger = CHILD2(probe);
		if(CHILD2(probe)<heap->used) {
			if(items[CHILD1(probe)].a>items[CHILD2(probe)].a) {
				larger = CHILD1(probe);
			}
		}
		if(items[probe].a>items[larger].a) break;
		thing_t temp = items[probe];
		items[probe] = items[larger];
		items[larger] = temp;
		
		probe = larger;
	}
}


void heap_print(heap_t *heap)
{
	for(int i=0; i< heap->used; i++) {
		printf("%3d: %3d\n",i, heap->items[i].a);
	}
	printf("\n");
}

