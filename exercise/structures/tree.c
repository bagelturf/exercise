//
//  tree.c
//  exercise
//
//  Created by Steve Weller on 3/10/19.
//  Copyright © 2019 Steve Weller. All rights reserved.
//

#include "tree.h"

#include <stdlib.h>
#include <stdio.h>

pm create_tree_node(void)
{
	return calloc(sizeof(struct m), 1);
}

void destroy_tree_node(pm node)
{
	free(node);
}

typedef void (^enumerate_tree_b)(pm node, int level);

void enumerate_tree_depth_recursive(pm *t, int level, enumerate_tree_b block)
{
	if(!*t) return;
	enumerate_tree_depth_recursive(&(*t)->left, level+1, block);
	block(*t, level);
	enumerate_tree_depth_recursive(&(*t)->right, level+1, block);
}


pm stack[100];
int stack_top = -1;

void stack_push(pm node)
{
	stack_top++;
	stack[stack_top] = node;
}

int stack_pop(pm *node)
{
	if(stack_top<0) return 0;
	
	*node = stack[stack_top];
	stack_top--;
	
	return 1;
}

// Handles level incorrectly
void enumerate_tree_depth_stack(pm *t, int level, enumerate_tree_b block)
{
	if(!*t) return;
	
	stack_push(*t);
	
	// pop node
	// If node->left, push right (if exists), push self block, push left
	// else block self
	
	pm node = NULL;
	while(stack_pop(&node)) {
		if(node->right) stack_push(node->right);
		block(node, level);
		if(node->left) stack_push(node->left);
	};
}



void build_tree_random_recursive(pm *root, int level, int max_depth)
{
	if(level==max_depth) return;
	*root = create_tree_node();
	(*root)->value = arc4random_uniform(100);
	
	if(arc4random_uniform(100)<60) {
		build_tree_random_recursive(&(*root)->left, level+1, max_depth);
	}
	if(arc4random_uniform(100)<60) {
		build_tree_random_recursive(&(*root)->right, level+1, max_depth);
	}
}


void print_tree_stack(pm *root)
{
	static char *indent_string = "    ";
	enumerate_tree_depth_stack(root, 0, ^(pm node, int level) {
		printf("<%p> %3d: ",node, level);
		while (level--) {
			printf("%s",indent_string);
		}
		printf("%g\n", node->value);
	});
}

void print_tree_recursive(pm *root)
{
	static char *indent_string = "    ";
	enumerate_tree_depth_recursive(root, 0, ^(pm node, int level) {
		printf("<%p> %3d: ",node, level);
		while (level--) {
			printf("%s",indent_string);
		}
		printf("%g\n", node->value);
	});
}


