//
//  nonrepeat.c
//  exercise
//
//  Created by Steve Weller on 3/10/19.
//  Copyright © 2019 Steve Weller. All rights reserved.
//

#include "nonrepeat.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAP_SIZE 256

void non_repeat(const char *s)
{
	int *map = calloc(MAP_SIZE, sizeof(int));
	
	// Get char counts
	char c;
	const char *p = s;
	while((c=*p++)) {
		map[c]++;
	}
	
	p = s;
	while((c=*p++)) {
		if(map[c]>=2) {
			printf("'%s': First is %c\n",s,c);
			break;
		}
	}
	
	if(!c) {
		printf("'%s': No repeat\n",s);
	}
	
	free(map);
}


void non_repeat_bits(const char *s)
{
	uint8_t *once_map = calloc(MAP_SIZE/8, sizeof(uint8_t));
	uint8_t *twice_map = calloc(MAP_SIZE/8, sizeof(uint8_t));
	
	// Get char counts
	char c;
	const char *p = s;
	while((c=*p++)) {
		if((once_map[c/8] & (1<<(c%8)))==0) {
			once_map[c/8] |= 1<<(c%8);
		} else {
			twice_map[c/8] |= 1<<(c%8);
		}
	}
	
	p = s;
	while((c=*p++)) {
		if((twice_map[c/8] & (1<<(c%8)))!=0) {
			printf("'%s': First is %c\n",s,c);
			break;
		}
	}
	
	if(!c) {
		printf("'%s': No repeat\n",s);
	}
	
	free(once_map);
	free(twice_map);
}


void non_repeat_bits_single(const char *s)
{
	uint8_t *map = calloc(MAP_SIZE/8, sizeof(uint8_t));
	
	// Find out where the end is so we can go backwards
	size_t len = strlen(s);
	char first_repeated = '\0';
	
	// Build map as we move to the front
	char c;
	const char *p = s+len-1;
	while((c=*p--)) {
		if((map[c/8] & (1<<(c%8)))!=0) {
			first_repeated = c;
		} else {
			map[c/8] |= 1<<(c%8);
		}
	}
	
	if(!first_repeated) {
		printf("'%s': No repeat\n",s);
	} else {
		printf("'%s': First is %c\n",s,first_repeated);
	}
	
	free(map);
}

