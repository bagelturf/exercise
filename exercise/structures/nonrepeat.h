//
//  nonrepeat.h
//  exercise
//
//  Created by Steve Weller on 3/10/19.
//  Copyright © 2019 Steve Weller. All rights reserved.
//

#ifndef nonrepeat_h
#define nonrepeat_h


void non_repeat(const char *s);
void non_repeat_bits(const char *s);
void non_repeat_bits_single(const char *s);


#endif /* nonrepeat_h */
