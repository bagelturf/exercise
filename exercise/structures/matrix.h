//
//  matrix.h
//  exercise
//
//  Created by Steve Weller on 3/10/19.
//  Copyright © 2019 Steve Weller. All rights reserved.
//

#ifndef matrix_h
#define matrix_h

#include "element.h"

element *matrix_create(int nc, int nr);

typedef void (^matrix_enumerator_b)(element *e, int c, int r);

void enumerate_perimeter(element *m, int nc, int nr, int row_inc, matrix_enumerator_b block);

void matrix_rotate(element *m, int nc, int nr);
void matrix_rotate_in_place(element *m, int nc, int nr);

void matrix_fill_value(element *m, int nc, int nr);
void matrix_print(element *m, int nc, int nr);


#endif /* matrix_h */
