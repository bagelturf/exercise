//
//  thing.h
//  exercise
//
//  Created by Steve Weller on 3/10/19.
//  Copyright © 2019 Steve Weller. All rights reserved.
//

#ifndef thing_h
#define thing_h


typedef struct thing {
	int a;
	int b;
} thing_t;


#endif /* thing_h */
