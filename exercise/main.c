//
//  main.c
//  exercise
//
//  Created by Steve Weller on 3/11/15.
//  Copyright (c) 2015 Steve Weller. All rights reserved.
//

#include <stdio.h>

#pragma mark Main

#include "thing.h"
#include "heap.h"
#include "binarychop.h"
#include "nonrepeat.h"
#include "tree.h"
#include "single.h"
#include "matrix.h"


int main(int argc, const char * argv[]) {

#pragma mark Heap
	printf("Heap\n");

	heap_t heap = { };
	thing_t thing1 = { 1, 1 };
	thing_t thing2 = { 3, 9 };
	thing_t thing3 = { 2, 4 };
	thing_t thing4 = { 4, 16 };
	thing_t thing5 = { 5, 25 };
	
	heap_add_thing(&heap, thing1);
	heap_print(&heap);
	heap_add_thing(&heap, thing2);
	heap_print(&heap);
	heap_add_thing(&heap, thing3);
	heap_print(&heap);
	heap_add_thing(&heap, thing4);
	heap_print(&heap);
	thing_t removed_thing;
	heap_remove_thing(&heap, &removed_thing);
	heap_print(&heap);
	heap_add_thing(&heap, thing5);
	heap_print(&heap);
	

#pragma mark Bin search
	
	printf("\n");
	printf("Binary chop\n");
	thing_t things[] = {  { 1, 2}, {3, 4}, { 5, 6}, {7, 8}};

	thing_t target = { 5, 4 };
	thing_t *found_thing = bin_search(things, sizeof(thing_t), sizeof(things)/sizeof(thing_t), &target, ^int(void *thing1, void *thing2) {
		thing_t *t1 = thing1;
		thing_t *t2 = thing2;
		
		if(t1->a > t2->a) return 1;
		if(t1->a < t2->a) return -1;
		return 0;
	});
	
	printf("Found thing: %d\n",found_thing==NULL ? -1 : found_thing->a);
	
	
#pragma mark - Non-repeat
	
	printf("\n");
	printf("Non-repeated string\n");
	char *repeat_string = "abcefgdstd";
	non_repeat(repeat_string);
	non_repeat_bits(repeat_string);
	non_repeat_bits_single(repeat_string);
	
	
#pragma Tree
	
	printf("\n");
	printf("Tree\n");
	pm root;
	
	build_tree_random_recursive(&root, 0, 7);
	print_tree_recursive(&root);
//	print_tree_stack(&root);

	
#pragma mark Linked single
	
	printf("\n");
	printf("Single-linked list\n");
	single_node_p head = NULL, tail = NULL;
	
	print_list_recursive(&head, &tail);
	
	push_head(&head, &tail, 1.0);
	print_list_recursive(&head, &tail);
	
	push_tail(&head, &tail, 2.0);
	print_list_recursive(&head, &tail);

	push_head(&head, &tail, 7.0);
	print_list_recursive(&head, &tail);

	push_head(&head, &tail, 20.0);
	print_list_recursive(&head, &tail);

	element value = 0.0;
	int success = 0;
	
	success = pop_head(&head, &tail, &value);
	printf("Success: %d value: %15.10g\n", success, value);
	print_list_recursive(&head, &tail);
	
	success = pop_head(&head, &tail, &value);
	printf("Success: %d value: %15.10g\n", success, value);
	print_list_recursive(&head, &tail);
	
	success = pop_head(&head, &tail, &value);
	printf("Success: %d value: %15.10g\n", success, value);
	print_list_recursive(&head, &tail);

	success = pop_head(&head, &tail, &value);
	printf("Success: %d value: %15.10g\n", success, value);
	print_list_recursive(&head, &tail);
	
	
#pragma mark Matrix
	
	printf("\n");
	printf("Matrix\n");

	int nr = 10;
	int nc = 10;
	element *m = matrix_create(nc, nr);
	
	matrix_fill_value(m, nc, nr);
	matrix_print(m, nc, nr);
	
	printf("\n");
	
	__block int fill_value = -1;
	int inset = 0;
	do {
		int inset_nc = nc-inset-inset;
		int inset_nr = nr-inset-inset;
		if(inset_nc<=0 || inset_nr<=0) break;
		element *inset_m = m+inset*nc+inset;
		enumerate_perimeter(inset_m, inset_nc, inset_nr, nc, ^(element *e, int c, int r) {
			*e = fill_value--;
		});
		inset++;
	} while(1);

	matrix_print(m, nc, nr);
	printf("\n");
	
//	matrix_rotate(m, nc, nr);
	matrix_rotate_in_place(m, nc, nr);
	matrix_print(m, nc, nr);
	printf("\n");

	
	return 0;
}
